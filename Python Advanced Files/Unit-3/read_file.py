def read_file(file_name):
    msg = "__CONTENT_START__\n"
    try:
        file = open(file_name, 'r')
    except OSError:
        msg += "__NO_SUCH_FILE__\n"
    else:
        msg += file.read()
    finally:
        msg += "__CONTENT_END__\n"
        return msg

