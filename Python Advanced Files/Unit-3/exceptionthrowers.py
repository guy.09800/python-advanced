def f1():
    x = [1, 2]
    y = iter(x)
    print(y.__next__())
    print(y.__next__())
    print(y.__next__())
    print(y.__next__())


def f2():
    number = 2 / 0


def f3():
    n1 = 2
    n2 = 0
    assert n2 != 0
    return n1 / n2


def f4():
    my_dict = {}
    my_name = my_dict["G"]



def f5():
     print("Indentation Error")


def f6():
    my_num = "G" / 5


