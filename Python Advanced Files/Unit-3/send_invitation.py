class UnderAge(Exception):
    def __init__(self, age):
        self.age = age
        
    def __str__(self):
        return f"You are under 18 years old. Your age is {self.age}. You can come to the party in {18-self.age} years."

def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(age)
        else:
            print("You should send an invite to " + name)
    except UnderAge as e:
        print(e)


send_invitation("Guy", 19)
