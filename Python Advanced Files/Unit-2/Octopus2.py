class octopus:
    count_animals = 0
    def __init__(self, name="Octavio"):
        self._name = name
        self._age = 8
        octopus.count_animals += 1
    
    def birthday(self):
        self._age += 1
    
    def get_age(self):
        return self._age

    def set_name(self, name):
        self._name = name
    
    def get_name(self):
        return self._name
    

    

def main():
    o1 = octopus()
    o2 = octopus("octo1")
    print(o1.get_name())
    print(o2.get_name())
    o1.set_name("octo2")
    print(o1.get_name())
    print(octopus.count_animals)

main()
