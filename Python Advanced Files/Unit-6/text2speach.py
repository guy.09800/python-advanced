import pyttsx3


def main():
    engine = pyttsx3.init()

    # Set the properties
    engine.setProperty('rate', 150)  
    engine.setProperty('volume', 0.8)  
    voices = engine.getProperty('voices')

    # Convert text to speech
    text = "Text To Speach in python"
    engine.say(text)

    engine.runAndWait()


if __name__ == '__main__':
    main()
