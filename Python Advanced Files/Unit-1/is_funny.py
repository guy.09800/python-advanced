# 1.3.3

def is_funny(string : str) -> bool:
    return all(ch in 'ha' for ch in string)