# 1.1.4

from functools import reduce

def sum_of_digits(number):
    return reduce(lambda x, y: x + int(y), str(number), 0)