# 1.5.3

with open('UNIT-1\\names.txt', 'r') as f:
    shortest = min([len(name.strip()) for name in f])
    f.seek(0)
    print('\n'.join([name.strip() for name in f if len(name.strip()) == shortest]))
