# 1.1.2

def duplicate(let : str) -> str:
    return let * 2

# could be implemented using lambda
def double_letter(my_str : str) -> str:
    return ''.join(map(duplicate, my_str))

