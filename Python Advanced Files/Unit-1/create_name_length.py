# 1.5.4

with open('UNIT-1\\names.txt', 'r') as f, open('name_length.txt', 'w') as f2:
    lengths = [len(name.strip()) for name in f]
    f2.write('\n'.join(map(str, lengths)))
