# 1.3.2
import math

def is_prime(number : int) -> bool:
    return number > 1 and [x for x in range(2,int(math.sqrt(number))) if number % x == 0] == []

print(is_prime(42))