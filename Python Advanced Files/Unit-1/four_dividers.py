# 1.1.3

def four_dividers(number : int) -> list:
    return [num for num in range(1,number) if num % 4 == 0]


print(four_dividers(9))