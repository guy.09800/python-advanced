# 1.5.5

length = int(input('Enter name length: '))
with open('UNIT-1\\names.txt', 'r') as f:
    [print(name.strip()) for name in f if len(name.strip()) == length]
