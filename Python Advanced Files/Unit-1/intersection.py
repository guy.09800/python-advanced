
# 1.3.1

def intersection(list_1 : list, list_2 : list) -> list:
    return list(set([x for x in list_1 + list_2 if x in list_1 and x in list_2]))

print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))