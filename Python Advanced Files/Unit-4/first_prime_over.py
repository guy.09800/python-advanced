import math

def is_prime(n):
    if n <= 1:
        return False
    for i in range(2, int(math.sqrt(n))):
        if n % i == 0:
            return False
    return True


def first_prime_over(n):
    prime_gen = (number for number in range(n + 1, 2 * n) if is_prime(number))
    return next(prime_gen)


def main():
    print(first_prime_over(1000000))

if __name__ == '__main__':
    main()

