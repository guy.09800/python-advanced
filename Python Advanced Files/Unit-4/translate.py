def translate(sentence):
    dict = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    translation_generator = (dict[word] for word in sentence.split(" "))
    translated_sentence = ""
    for word in translation_generator:
        translated_sentence += word + " "
    return translated_sentence


def main():
    print(translate("el gato esta en la casa"))

