def get_fibo():
    x = 0
    y = 1
    yield x
    yield y
    while True:
        yield x + y
        x, y = y, x + y


def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))


if __name__ == '__main__':
    main()
