def gen_secs():
    for second in range(60):
        yield second


def gen_minutes():
    for minute in range(60):
        yield minute


def gen_hours():
    for hour in range(24):
        yield hour


def gen_time():
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                yield f"{hour:02d}:{minute:02d}:{second:02d}"


def gen_years(start=2023):
    while True:
        yield start
        start += 1


def gen_months():
    for month in range(1, 13):
        yield month


def gen_days(month, leap_year=True):
    if leap_year:
        if month == 2:
            for day in range(1, 30):
                yield day
        elif month % 2 == 0:
            for day in range(1, 31):
                yield day
        else:
            for day in range(1, 32):
                yield day
    else:
        if month == 2:
            for day in range(1, 29):
                yield day
        elif month % 2 == 0:
            for day in range(1, 31):
                yield day
        else:
            for day in range(1, 32):
                yield day


def gen_date():
    for year in gen_years():
        for month in gen_months():
            for day in gen_days(month, year % 4 == 0 and year % 100 != 0):
                for time in gen_time():
                    yield f"{day:02d}/{month:02d}/{year:02d} " + time


def main():
    best_date = gen_date()
    counter = 0
    while True:
        current_iteration = next(best_date)
        if counter == 1000000:
            counter = 0
            print(current_iteration)
        counter += 1


if __name__ == '__main__':
    main()
