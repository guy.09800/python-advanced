import itertools

def get_all_options():
    money = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
    return len(set([seq for i in range(7, len(money)) for seq in itertools.combinations(money, i)
                    if sum(seq) == 100]))


def main():
    print(get_all_options())


if __name__ == '__main__':
    main()
